<?php
namespace Sebwite\Tests\Phpstorm;

use Sebwite\Testbench\Traits\ServiceProviderTester;

class PhpstormServiceProviderTest extends TestCase
{
    use ServiceProviderTester;
}
