<?php

namespace Sebwite\Tests\Phpstorm;

use Sebwite\Testbench\AbstractTestCase;

abstract class TestCase extends AbstractTestCase
{
    /**
     * {@inheritdoc}
     */
    protected function getServiceProviderClass($app)
    {
        return \Sebwite\Phpstorm\PhpstormServiceProvider::class;
    }
}
