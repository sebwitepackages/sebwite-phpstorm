<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Phpstorm;

use Illuminate\Contracts\Foundation\Application;
use Sebwite\Support\ServiceProvider;

/**
 * This is the PhpstormServiceProvider.php.
 *
 * @author    Sebwite Dev Team
 * @copyright Copyright (c) 2015, Sebwite
 * @license   https://tldrlegal.com/license/mit-license MIT License
 */
class PhpstormServiceProvider extends ServiceProvider
{
    /**
     * @var string
     */
    protected $dir = __DIR__;

    protected $configFiles = [ 'phpstorm' ];

    protected $viewDirs = [ 'views' => 'phpstorm' ];

    /**
     * @var array
     */
    protected $singletons = [
        'phpstorm'       => Phpstorm::class,
        'phpstorm.metas' => MetaRepository::class
    ];

    /**
     * @var array
     */
    protected $aliases = [
        'phpstorm'       => Contracts\Phpstorm::class,
        'phpstorm.metas' => Contracts\MetaRepository::class
    ];

    protected $commands = [
        Console\MetaCommand::class
    ];

    public function register()
    {
        $app = parent::register();
        $app->resolving('phpstorm', function (Phpstorm $phpstorm, Application $app) {
        

            $phpstorm
                ->registerComponent('vcs', Components\VCS::class)
                ->registerComponent('workspace', Components\Workspace::class)
                ->registerComponent('laravel', Components\LaravelPlugin::class)
                ->registerComponent('folders', Components\Folders::class);
        });
    }
}
