<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Phpstorm\Metas;

use Illuminate\Contracts\Foundation\Application;
use Sebwite\Support\Arr;

/**
 * This is the ConfigMeta.
 *
 * @package        Sebwite
 * @author         Sebwite Dev Team
 * @copyright      Copyright (c) 2015, Sebwite
 * @license        https://tldrlegal.com/license/mit-license MIT License
 */
class BindingsMeta extends BaseMeta
{
    protected $app;

    protected $methods = [
        'new \Illuminate\Contracts\Container\Container',

        '\Illuminate\Contracts\Container\Container::make(\'\')',
        '\App::make(\'\')',
        'app(\'\')',
    ];

    /**
     * BindingsMeta constructor.
     *
     * @param $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function fatal_handler()
    {
        $errfile = "unknown file";
        $errstr  = "shutdown";
        $errno   = E_CORE_ERROR;
        $errline = 0;

        $error = error_get_last();

        if ($error !== null) {
            $errno   = $error[ "type" ];
            $errfile = $error[ "file" ];
            $errline = $error[ "line" ];
            $errstr  = $error[ "message" ];


            #$error = error_get_last();
            print_r($error, true);
            print_r(debug_backtrace(false), true);
        }
    }

    public function getData()
    {
        spl_autoload_register(function ($class) {

            throw new \Exception("Class '$class' not found.");
        });


        register_shutdown_function($this->app->wrap(static::class . "@fatal_handler"));

        $bindings             = [ ];
        $bindings[ 'config' ] = get_class($this->app->make('config'));

        foreach ($this->getBindingsAbstracts() as $abstract) {
            try {
                $concrete = $this->app->make($abstract);
                if (is_object($concrete)) {
                    $bindings[ $abstract ] = get_class($concrete);
                }
            } catch (\Exception $e) {
                //throw $e;
            } catch (\ErrorException $e) {
            }
        }

        return $bindings;
    }

    /**
     * Get a list of abstracts from the Laravel Application.
     *
     * @return array
     */
    protected function getBindingsAbstracts()
    {
        $abstracts = app()->getBindings();

        // Return the abstract names only
        $names = array_keys($abstracts);
        return Arr::without([ 'Illuminate\Database\Seeder' ], $names);
    }
}
