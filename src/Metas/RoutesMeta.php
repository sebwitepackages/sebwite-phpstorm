<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Phpstorm\Metas;

use Illuminate\Contracts\Container\Container;
use Illuminate\Routing\Router;

/**
 * This is the ConfigMeta.
 *
 * @package        Sebwite
 * @author         Sebwite Dev Team
 * @copyright      Copyright (c) 2015, Sebwite
 * @license        https://tldrlegal.com/license/mit-license MIT License
 */
class RoutesMeta extends BaseMeta
{
    protected $methods = [
        'route(\'\')',
        '\Illuminate\Routing\RouteCollection::getByName(\'\')',
        '\\URL::route(\'\')',
        'new \Illuminate\Contracts\Routing\UrlGenerator',
        '\Illuminate\Contracts\Routing\UrlGenerator::route(\'\')'
    ];

    protected $router;

    /**
     * RoutesMeta constructor.
     *
     * @param $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }


    public function getData()
    {
        $routes = [ ];
        /** @var \Illuminate\Routing\Route[] */
        $_routes = $this->router->getRoutes()->getRoutes();
        foreach ($_routes as $route) {
            $routes[ $route->getName() ] = false; //$route->getUri();
        }

        return $routes;
    }
}
