<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Phpstorm\Metas;

use Sebwite\Phpstorm\Contracts\Meta as MetaContract;

/**
 * This is the ConfigMeta.
 *
 * @package        Sebwite
 * @author         Sebwite Dev Team
 * @copyright      Copyright (c) 2015, Sebwite
 * @license        https://tldrlegal.com/license/mit-license MIT License
 */
abstract class BaseMeta implements MetaContract
{
    protected $template = <<<'EOF'
@foreach($methods as $method)
    {!! $method !!} => [
        '' == '@',
        @foreach($data as $k => $v)
            '{!! $k !!}' instanceof {!! \Sebwite\Support\Str::ensureLeft(is_string($v) && class_exists($v, false) ? $v : 'null', '\\') !!},
        @endforeach
    ],
@endforeach
EOF;

    protected $methods = [];

    public function getMethods()
    {
        return $this->methods;
    }

    public function getTemplate()
    {
        return $this->template;
    }
}
