<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Phpstorm;

use ArrayAccess;
use Illuminate\Contracts\Container\Container;
use IteratorAggregate;
use Sebwite\Phpstorm\Contracts\MetaRepository;
use Sebwite\Phpstorm\Contracts\Phpstorm as PhpstormContract;
use Sebwite\Support\Path;
use Sebwite\Support\Traits\DotArrayObjectTrait;
use Sebwite\Support\Traits\DotArrayTrait;

/**
 * This is the Factory.
 *
 * @package        Sebwite
 * @author         Sebwite Dev Team
 * @copyright      Copyright (c) 2015, Sebwite
 * @license        https://tldrlegal.com/license/mit-license MIT License
 *
 * @property Components\VCS           $vcs
 * @property Components\Workspace     $workspace
 * @property Components\LaravelPlugin $laravel
 * @property Components\Folders       $folders
 */
class Phpstorm implements PhpstormContract, ArrayAccess, IteratorAggregate
{
    use DotArrayTrait, DotArrayObjectTrait;

    /**
     * @var \Sebwite\Phpstorm\Contracts\MetaRepository
     */
    protected $metas;

    /**
     * @var \Illuminate\Contracts\Container\Container
     */
    protected $container;

    /**
     * @var string
     */
    protected $projectPath;

    /**
     * @var string
     */
    protected $ideaPath;

    protected $components = [ ];

    /**
     * Phpstorm constructor.
     *
     * @param \Illuminate\Contracts\Container\Container  $container
     * @param \Sebwite\Phpstorm\Contracts\MetaRepository $metas
     */
    public function __construct(Container $container, MetaRepository $metas)
    {
        $this->container   = $container;
        $this->metas       = $metas;
        $this->projectPath = $this->config('project_path');
        $this->ideaPath    = $this->config('idea_path');
    }

    /**
     * config method
     *
     * @param      $key
     * @param null $default
     * @return mixed
     */
    public function config($key, $default = null)
    {
        return array_get($this->container->make('config')->get('phpstorm'), $key, $default);
    }

    /**
     * registerComponent method
     *
     * @param $name
     * @param $class
     * @return static
     */
    public function registerComponent($name, $class)
    {
        $this->components[ $name ] = $class;

        return $this;
    }

    public function isRegisteredComponent($name)
    {
        return array_key_exists($name, $this->components);
    }

    public function component($name)
    {
        if (! $this->isRegisteredComponent($name)) {
            throw new \InvalidArgumentException("Component {$name} could not be found.");
        }

        return $this->container->make($this->components[ $name ]);
    }

    public function getProjectName()
    {
        return $this->container->make('files')->get(
            Path::join($this->getIdeaPath(), '.name')
        );
    }

    /**
     * get projectPath value
     *
     * @return mixed
     */
    public function getProjectPath()
    {
        return $this->projectPath;
    }

    /**
     * Set the projectPath value
     *
     * @param mixed $projectPath
     * @return Phpstorm
     */
    public function setProjectPath($projectPath)
    {
        $this->projectPath = $projectPath;

        return $this;
    }

    /**
     * get ideaPath value
     *
     * @return mixed
     */
    public function getIdeaPath()
    {
        return $this->ideaPath;
    }

    /**
     * Set the ideaPath value
     *
     * @param mixed $ideaPath
     * @return Phpstorm
     */
    public function setIdeaPath($ideaPath)
    {
        $this->ideaPath = $ideaPath;

        return $this;
    }

    /**
     * get metas value
     *
     * @return \Sebwite\Phpstorm\Contracts\MetaRepository
     */
    public function getMetas()
    {
        return $this->metas;
    }

    /**
     * Set the metas value
     *
     * @param \Sebwite\Phpstorm\Contracts\MetaRepository $metas
     * @return Phpstorm
     */
    public function setMetas($metas)
    {
        $this->metas = $metas;

        return $this;
    }

    /**
     * Get array accessor.
     *
     * @return mixed
     */
    protected function getArrayAccessor()
    {
        return 'components';
    }

    /**
     * Dynamically access container services.
     *
     * @param  string $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->component($key);
    }
}
