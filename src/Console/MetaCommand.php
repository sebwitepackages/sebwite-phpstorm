<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Phpstorm\Console;

use Sebwite\Phpstorm\Contracts\MetaRepository;
use Sebwite\Support\Console\Command;

/**
 * This is the PhpstormMetaCommand.
 *
 * @package        Sebwite
 * @author         Sebwite Dev Team
 * @copyright      Copyright (c) 2015, Sebwite
 * @license        https://tldrlegal.com/license/mit-license MIT License
 */
class MetaCommand extends Command
{
    protected $signature = 'phpstorm:meta';

    protected $description = 'Write phpstorm meta file';

    protected $metas;

    /**
     * @param \Sebwite\Phpstorm\Contracts\MetaRepository|\Sebwite\Phpstorm\MetaRepository $metas
     */
    public function __construct(MetaRepository $metas)
    {
        parent::__construct();
        $this->metas = $metas;
    }


    /** Instantiates the class */
    public function handle()
    {
        foreach (config('phpstorm.metas') as $name => $class) {
            $this->metas->add($name, $class);
        }


        try {
            $this->metas->create();
        } catch (\Exception $e) {
            $this->comment("Cannot create meta file: " . $e->getMessage());
            print $e->getTraceAsString();
        }
    }
}
