<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Idea\Console;

use Sebwite\Idea\Components\Folders;
use Sebwite\Support\Path;
use Sebwite\Support\Str;

class FoldersCommand extends BaseCommand
{
    protected $signature = 'folders {action : "list" or "add" }';

    public function handle()
    {
        $action = $this->argument('action');
        switch ($action) {
            case 'list':
                $this->listFolders();
                break;
            case 'add':
                foreach (config('idea.folders') as $conf) {
                    $this->addFolders($conf);
                }
                break;
        }
    }

    protected function listFolders()
    {
        $found = [ ];
        foreach ($this->idea()->folders->all()->toArray() as $folder) {
            $found[] = [ $folder[ 'prefix' ], $folder[ 'type' ], $folder[ 'path' ], $this->idea()->folders->hasPath($folder[ 'path' ]) ? 'Yes' : 'No' ];
        }
        $this->table([ 'Prefix', 'Type', 'Path', 'Added' ], $found);
    }

    protected function addFolders(array $config = [ ])
    {
        $folders = $this->idea()->folders;
        $found   = [ ];
        if ($config[ 'type' ] === Folders::SOURCE) {
            foreach ($this->fs()->globule($config[ 'glob' ]) as $path) {
                $composerPath = Path::join($path, 'composer.json');
                if ($this->fs()->exists($composerPath)) {
                    $composer = collection(json_decode($this->fs()->get($composerPath), true));
                    foreach ($composer->get('autoload.psr-4', [ ]) as $namespace => $dir) {
                        $dir     = Path::join($path, $dir);
                        $found[] = [ Folders::SOURCE, $namespace, $dir ];
                        $folders->addSource(
                            Str::removeRight($namespace, '\\'),
                            Path::join($path, $dir)
                        );
                    }
                }
            }
        }
    }

    protected function getFolders()
    {
        $folders = [ ];
        foreach (config('idea.folders') as $config) {
            foreach ($this->fs()->globule($config[ 'glob' ]) as $path) {
                if ($config[ 'type' ] === Folders::SOURCE) {
                }
                $folders[] = [
                    'type' => $config[ 'type' ],
                    'glob' => $config[ 'glob' ],
                    'has'  => $this->idea()->folders->hasPath($path)
                ];
            }
        }
    }

    protected function getSourceFolder($path)
    {
        $folders      = [ ];
        $composerPath = Path::join($path, 'composer.json');
        if ($this->fs()->exists($composerPath)) {
            $composer = collection(json_decode($this->fs()->get($composerPath), true));
            foreach ($composer->get('autoload.psr-4', [ ]) as $namespace => $dir) {
                $folders[] = [
                    'namespace' => $namespace,
                    'path'      => Path::join($path, $dir),
                    'composer'  => $composer
                ];
            }
        }

    }
}
