<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Phpstorm;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Collection;
use Radic\BladeExtensions\Renderers\BladeStringRenderer;
use Sebwite\Phpstorm\Contracts\MetaRepository as MetaRepositoryContract;
use Sebwite\Support\Filesystem;

/**
 * This is the MetaRepository.
 *
 * @package        Sebwite
 * @author         Sebwite Dev Team
 * @copyright      Copyright (c) 2015, Sebwite
 * @license        https://tldrlegal.com/license/mit-license MIT License
 */
class MetaRepository extends Collection implements MetaRepositoryContract
{

    protected $container;

    /**
     * @var \Illuminate\Contracts\View\Factory
     */
    protected $views;

    /**
     * @var \Sebwite\Support\Filesystem
     */
    protected $files;

    protected $config;

    protected $blade;

    /**
     * Create a new collection.
     *
     * @param \Illuminate\Contracts\Container\Container $container
     * @param \Illuminate\Contracts\View\Factory        $views
     * @param \Illuminate\Contracts\Config\Repository   $config
     * @param \Sebwite\Support\Filesystem               $files
     * @internal param \Illuminate\Contracts\View\Factory $viewFactory
     * @internal param \Illuminate\Contracts\Config\Repository $configRepository
     * @internal param mixed $items
     */
    public function __construct(Container $container, Factory $views, Repository $config, Filesystem $files, BladeStringRenderer $blade)
    {
        $this->container = $container;
        $this->views     = $views;
        $this->config    = $config;
        $this->files     = $files;
        $this->blade     = $blade;

        parent::__construct([ ]);
    }

    public function add($name, $class)
    {
        if (! class_exists($class)) {
            throw new \FileNotFoundException("Could not find class $class");
        }
        $this->put($name, $class);
    }

    public function create($path = null, $viewFile = null)
    {
        $path     = is_null($path) ? $this->config->get('phpstorm.metaFile') : $path;
        $viewFile = is_null($viewFile) ? $this->config->get('phpstorm.metaView') : $viewFile;

        try {
            $metas = [ ];

            foreach ($this->all() as $name => $className) {
                if ($name === 'bitbucket' && ! class_exists('Bitbucket\API\Api')) {
                    continue;
                }
                /** @var Contracts\Meta $meta */
                $meta    = $this->container->make($className);
                $methods = $meta->getMethods();
                $data    = $meta->getData();
                $metas[] = $this->blade->render($meta->getTemplate(), compact('methods', 'data'));
            }

            $open    = '<?php';
            $content = $this->views->make($viewFile, compact('open', 'metas'))->render();

            $this->files->put($path, $content);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
