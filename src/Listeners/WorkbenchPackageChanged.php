<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Phpstorm\Listeners;

use Illuminate\Contracts\Console\Kernel;
use Sebwite\Phpstorm\Contracts\Phpstorm;
use Sebwite\Workbench\Events\PackageChanged;

/**
 * This is the class WorkbenchPackageChanged.
 *
 * @package        Sebwite\Phpstorm
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
class WorkbenchPackageChanged
{

    /**
     * @var \Sebwite\Phpstorm\Contracts\Phpstorm|\Sebwite\Phpstorm\Phpstorm
     */
    protected $phpstorm;

    protected $console;

    /**
     * Create the event listener.
     *
     * @param \Sebwite\Phpstorm\Contracts\Phpstorm|\Sebwite\Phpstorm\Phpstorm $phpstorm
     */
    public function __construct(Phpstorm $phpstorm, Kernel $console)
    {
        $this->phpstorm = $phpstorm;
        $this->console  = $console;
    }

    /**
     * Handle the event.
     *
     * @param  PackageChanged $event
     * @return void
     */
    public function handle(PackageChanged $event)
    {
        $path = $event->package[ 'path' ];

        if ($event->type === 'removed' && $this->phpstorm->vcs()->has($path)) {
            $params = [
                'path'   => $path,
                'action' => 'remove',
                'ask'    => true
            ];
            $this->console->call('phpstorm:vcs', $params);
        }
    }
}
