<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Phpstorm\Components;

use Illuminate\Filesystem\Filesystem;
use Sebwite\Phpstorm\Contracts\Phpstorm;
use Sebwite\Support\Str;

/**
 * This is the class LaravelPlugin.
 *
 * @package        Sebwite\Phpstorm
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
class Folders extends Component
{

    /**
     * Folders constructor.
     *
     * @param \Sebwite\Phpstorm\Contracts\Phpstorm|\Sebwite\Phpstorm\Phpstorm $phpstorm
     * @param \Illuminate\Filesystem\Filesystem                               $files
     */
    public function __construct(Phpstorm $phpstorm, Filesystem $files)
    {
        parent::__construct($phpstorm, $files, $phpstorm->getProjectName(), '.iml');
    }

    protected function getRootName()
    {
        return 'module';
    }

    /**
     * getContent method
     *
     * @return \SimpleXMLElement
     */
    protected function getContent()
    {
        return $this->getXml()->component->content;
    }

    protected function getFolders()
    {
        $folders = [ ];
        foreach ($this->getContent()->sourceFolder as $folder) {
            $a          = $this->getAttributes($folder);
            $a[ 'url' ] = $this->cleanPath(Str::removeLeft($a[ 'url' ], 'file://'));
            $folders[]  = $a;
        }

        return $folders;
    }

    public function has($path)
    {
        return ! collect($this->getFolders())->where('url', $path)->isEmpty();
    }

    public function hasPrefix($prefix)
    {
        return ! collect($this->getFolders())->where('packagePrefix', $prefix)->isEmpty();
    }

    public function add($prefix, $path, $isTest = false)
    {
        $child = $this->getContent()->addChild('sourceFolder');
        $child->addAttribute('packagePrefix', $prefix);
        $child->addAttribute('url', 'file://' . $this->parsePath($path));
        $child->addAttribute('isTestSource', $isTest ? 'true' : 'false');

        return $this;
    }

    /**
     * all method
     *
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        return collect($this->getFolders())->transform(function ($v) {

            return [
                'path'   => $v[ 'url' ],
                'prefix' => isset($v[ 'packagePrefix' ]) ? $v[ 'packagePrefix' ] : '',
                'test'   => isset($v[ 'isTestSource' ]) && $v[ 'isTestSource' ] === 'true'
            ];
        });
    }

    public function get($path)
    {
        return $this->all()->where('path', $path)->first();
    }
}
