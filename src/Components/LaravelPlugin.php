<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Phpstorm\Components;

use Illuminate\Filesystem\Filesystem;
use Sebwite\Phpstorm\Contracts\Phpstorm;

/**
 * This is the class LaravelPlugin.
 *
 * @package        Sebwite\Phpstorm
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
class LaravelPlugin extends Component
{
    public function __construct(Phpstorm $phpstorm, Filesystem $files)
    {
        parent::__construct($phpstorm, $files, 'laravel-plugin');
    }

    protected function getRootName()
    {
        return 'project';
    }

    /**
     * getTemplatePaths method
     *
     * @return \SimpleXMLElement
     */
    protected function getTemplatePaths()
    {
        return $this->getComponent('LaravelPluginSettings')->xpath('//option[@name="templatePaths"]/list/templatePath');
    }

    public function viewPaths()
    {
        $paths = [ ];
        foreach ($this->getTemplatePaths() as $path) {
            $paths[] = $this->getAttributes($path);
        }

        return $paths;
    }


    public function hasPath($path)
    {
        return ! collect($this->viewPaths())->where('path', $path)->isEmpty();
    }

    public function hasNamespace($namespace)
    {
        return ! collect($this->viewPaths())->where('namespace', $namespace)->isEmpty();
    }

    /**
     * getPath method
     *
     * @param $path
     * @return \SimpleXMLElement[]
     */
    public function getPath($path)
    {
        // $this->getComponent('LaravelPluginSettings')->xpath('//option[@name="templatePaths"]/list/templatePath[@path="' . $path . '"]');
        return collect($this->viewPaths())->where('path', $path)->first();
    }

    public function getNamespace($namespace)
    {
        return ! collect($this->viewPaths())->where('namespace', $namespace)->first();
    }

    public function add($namespace, $path)
    {
        if (count($this->getTemplatePaths()) === 0) {
            $option = $this->getComponent('LaravelPluginSettings')->addChild('option');
            $option->addAttribute('name', 'templatePaths');
            $list = $option->addChild('list');
        } else {
            $list = $this->getComponent('LaravelPluginSettings')->xpath('//option[@name="templatePaths"]/list')[ 0 ];
        }
        $child = $list->addChild('templatePath');
        $child->addAttribute('namespace', $namespace);
        $child->addAttribute('path', $path);
    }
}
