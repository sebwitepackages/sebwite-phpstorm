<?php
/**
 * Part of the Docit PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Phpstorm\Components;

use Illuminate\Filesystem\Filesystem;
use Sebwite\Phpstorm\Contracts\Phpstorm;

/**
 * This is the class Workspace.
 *
 * @package        Sebwite\Phpstorm
 * @author         Docit
 * @copyright      Copyright (c) 2015, Docit. All rights reserved
 */
class Workspace extends Component
{
    /**
     * Workspace constructor.
     *
     * @param \Sebwite\Phpstorm\Contracts\Phpstorm $phpstorm
     * @param \Illuminate\Filesystem\Filesystem    $files
     */
    public function __construct(Phpstorm $phpstorm, Filesystem $files)
    {
        parent::__construct($phpstorm, $files, 'workspace');
    }

    protected function getRootName()
    {
        return 'project';
    }


    /**
     * getRunManager method
     *
     * @return \SimpleXMLElement
     */
    protected function getRunManager()
    {
        return $this->getComponent('RunManager', true);
    }

    /**
     * addRunConfiguration method
     *
     * @param        $name
     * @param string $type
     * @param        $factoryName
     * @return \SimpleXMLElement
     */
    protected function addRunConfiguration($name, $type, $factoryName)
    {
        $c = $this->getRunManager()->addChild('configuration');
        $c->addChild('method');
        $c->addAttribute('default', 'false');
        $c->addAttribute('name', $name);
        $c->addAttribute('type', $type);
        $c->addAttribute('factoryName', $factoryName);

        return $c;
    }

    /**
     * getRunConfigurations method
     *
     * @return array
     */
    public function getRunConfigs()
    {
        $confs = [ ];
        foreach ($this->getRunManager()->xpath('//configuration') as $conf) {
            if ($conf->count() === 0) {
                continue;
            }
            $attrs = $this->getAttributes($conf);

            if ($attrs[ 'default' ] == 'true') {
                continue;
            }
            if (isset($attrs[ 'path' ])) {
                $attrs[ 'path' ] = $this->cleanPath($attrs[ 'path' ]);
            }
            $confs[ $attrs[ 'name' ] ] = $attrs;
        }

        return $confs;
    }

    /**
     * addPhpRunConfig method
     *
     * @param $name
     * @param $path
     * @param $params
     * @return $this
     */
    public function addPhpRunConfig($name, $path, $params)
    {
        $c = $this->addRunConfiguration($name, 'PhpLocalRunConfigurationType', 'PHP Console');
        $c->addAttribute('path', $this->parsePath($path));
        $c->addAttribute('scriptParameters', $params);

        return $this;
    }

    /**
     * hasRunConfig method
     *
     * @param $name
     * @return bool
     */
    public function hasRunConfig($name)
    {
        return isset($this->getRunConfigs()[ $name ]);
    }
}
