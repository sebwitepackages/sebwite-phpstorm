<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Phpstorm\Components;

use Illuminate\Filesystem\Filesystem;
use Sebwite\Phpstorm\Contracts\Phpstorm;
use Sebwite\Support\Path;
use Sebwite\Support\Traits\Extendable;

/**
 * This is the class Reader.
 *
 * @package        Sebwite\Phpstorm
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
abstract class Component
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var \SimpleXMLElement
     */
    protected $xml;

    /** Instantiates the class
     *
     * @param \Sebwite\Phpstorm\Phpstorm|\Sebwite\Phpstorm\Contracts\Phpstorm $phpstorm
     * @param \Illuminate\Filesystem\Filesystem                               $files
     * @param  string                                                         $name
     * @throws \FileNotFoundException
     */
    public function __construct(Phpstorm $phpstorm, Filesystem $files, $name, $ext = '.xml')
    {
        $this->phpstorm = $phpstorm;
        $this->files    = $files;
        $this->name     = $name;
        $this->path     = Path::join($phpstorm->getIdeaPath(), $name . $ext);

        $this->ensureFile();
    }

    abstract protected function getRootName();

    /**
     * ensureFile method
     */
    protected function ensureFile()
    {
        if (! $this->files->exists($this->path)) {
            $rootName = $this->getRootName();
            $project  = simplexml_load_string("<{$rootName}></{$rootName}>");
            $project->addAttribute('version', '4');
            $this->writeXML($project);
            $this->xml = $project;
        } else {
            $this->xml = $this->readXML();
        }
    }

    /**
     * save method
     *
     * @return $this
     */
    public function save()
    {
        $this->writeXML($this->xml);

        return $this;
    }

    /**
     * reload method
     *
     * @return $this
     */
    public function reload()
    {
        $this->xml = $this->readXML();

        return $this;
    }

    /**
     * addComponent method
     *
     * @param $name
     * @return $this
     */
    protected function addComponent($name)
    {
        $this->xml->addChild('component')->addAttribute('name', $name);

        return $this;
    }

    /**
     * getComponent method
     *
     * @param $name
     * @return \SimpleXMLElement
     */
    protected function getComponent($name, $ensure = false)
    {
        if ($ensure === true && ! $this->hasComponent($name)) {
            $this->addComponent($name);
        }

        foreach ($this->xml->xpath('//component') as $comp) {
            if ((string)$comp->attributes()->{'name'} == $name) {
                return $comp;
            }
        }
    }

    /**
     * hasComponent method
     *
     * @param $name
     * @return bool
     */
    protected function hasComponent($name)
    {
        foreach ($this->xml->xpath('//component') as $comp) {
            if ((string)$comp->attributes()->{'name'} == $name) {
                return true;
            }
        }

        return false;
    }

    /**
     * readFile method
     *
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function readFile()
    {
        return $this->files->get($this->path);
    }

    /**
     * writeFile method
     *
     * @param $data
     * @return $this
     */
    protected function writeFile($data)
    {
        $this->files->put($this->path, $data);

        return $this;
    }

    /**
     * readXML method
     *
     * @return \SimpleXMLElement
     */
    protected function readXML()
    {
        return simplexml_load_string($this->readFile());
    }

    /**
     * writeXML method
     *
     * @param \SimpleXMLElement $xml
     * @return $this
     */
    protected function writeXML(\SimpleXMLElement $xml)
    {
        $this->writeFile($xml->asXML());

        return $this;
    }

    /**
     * get xml value
     *
     * @return \SimpleXMLElement
     */
    protected function getXml()
    {
        return $this->xml;
    }

    /**
     * Set the xml value
     *
     * @param \SimpleXMLElement $xml
     * @return Component
     */
    protected function setXml($xml)
    {
        $this->xml = $xml;

        return $this;
    }

    protected function getAttributes(\SimpleXMLElement $xml)
    {
        $attrs = [ ];
        foreach ($xml->attributes() as $k => $v) {
            $attrs[ $k ] = (string)$v;
        }

        return $attrs;
    }

    /**
     * parsePath method
     *
     * @param      $path
     * @param null|string $var
     * @return string
     */
    protected function parsePath($path, $var = null)
    {
        if ($var === null) {
            $var = '$' . strtoupper($this->getRootName()) . '_DIR$';
        }
        $path = Path::isRelative($path) ? $path : Path::makeRelative($path, base_path());
        $path = Path::join($var, $path);

        return $path;
    }

    /**
     * cleanPath method
     *
     * @param      $path
     * @param null|string $var
     * @return string
     */
    protected function cleanPath($path, $var = null)
    {
        if ($var === null) {
            $var = '$' . strtoupper($this->getRootName()) . '_DIR$';
        }

        return Path::makeRelative($path, $var);
    }
}
