<?php

return [
    'project_path' => base_path(),
    'idea_path'    => base_path('.idea'),
    'metaFile'     => base_path('.phpstorm.meta.php'),
    'metaView'     => 'phpstorm::meta',
    'metas'        => [
        'bindings' => 'Sebwite\Phpstorm\Metas\BindingsMeta',
        'config'   => 'Sebwite\Phpstorm\Metas\ConfigMeta',
        'routes'   => 'Sebwite\Phpstorm\Metas\RoutesMeta',
        'bitbucket' => 'Sebwite\Phpstorm\Metas\BitbucketMeta'
    ]
];
