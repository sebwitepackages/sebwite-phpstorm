Sebwite Phpstorm
====================

[![Build Status](https://img.shields.io/travis/sebwite/phpstorm.svg?&style=flat-square)](https://travis-ci.org/sebwite/phpstorm)
[![Scrutinizer coverage](https://img.shields.io/scrutinizer/coverage/g/sebwite/phpstorm.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/phpstorm)
[![Scrutinizer quality](https://img.shields.io/scrutinizer/g/sebwite/phpstorm.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/phpstorm)
[![Source](http://img.shields.io/badge/source-sebwite/phpstorm-blue.svg?style=flat-square)](https://github.com/sebwite/phpstorm)
[![License](http://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](https://tldrlegal.com/license/mit-license)

Sebwite Phpstorm is a package for the Laravel 5 framework.

The package follows the FIG standards PSR-1, PSR-2, and PSR-4 to ensure a high level of interoperability between shared PHP code.

Documentation
-------------
Tbd

Quick Installation
------------------
Begin by installing the package through Composer.

```bash
composer require sebwite/phpstorm
```

